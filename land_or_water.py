#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib.pyplot as plt
from functools import lru_cache

NX = 43200
NY = 21600

def land_or_water(long, lat):
    if (is_land(long, lat)):
        return 'Land'
    else:
        return 'Water'

@lru_cache(maxsize=None)
def load_data():
    data = np.fromfile('gl-latlong-1km-landcover.bsq', dtype=np.uint8)
    data = np.reshape(data, (NY,NX))
    return data

def get_nearest_index(long, lat):
    assert(long >= -180 and long <= 180)
    assert(lat >= -90 and lat <= 90)
    ix = round((long + 180) / 360 * (NX-1))
    iy = round((90 - lat) / 180 * (NY-1))
    return ix, iy

def is_land(long, lat, landcover=None):
    if (landcover is None): landcover = load_data()
    ix, iy = get_nearest_index(long, lat)
    return (landcover[iy,ix] != 0)

def show_map(landcover_data=None):
    if (landcover_data is None): landcover_data = load_data()
    plt.imshow(data[::50,::50])
    plt.xticks([])
    plt.yticks([])
    plt.show()

def test():
    print('Antarctica:', land_or_water(0, -90))
    print('Australia:', land_or_water(120.95, -24.56))
    print('Pacific:', land_or_water(-180, 0))

if (__name__ == '__main__'):
    if (len(sys.argv) == 1):
        test()
    else:
        assert(len(sys.argv) == 3)
        long = float(sys.argv[1])
        lat = float(sys.argv[2])
        print(land_or_water(long, lat))
